// Generated from oberon.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class oberonParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, T__25=26, T__26=27, T__27=28, ARRAY=29, OF=30, END=31, POINTER=32, 
		TO=33, RECORD=34, PROCEDURE=35, IN=36, IS=37, OR=38, DIV=39, MOD=40, NIL=41, 
		TRUE=42, FALSE=43, IF=44, THEN=45, ELSIF=46, ELSE=47, CASE=48, WHILE=49, 
		DO=50, REPEAT=51, UNTIL=52, FOR=53, BY=54, BEGIN=55, RETURN=56, CONST=57, 
		TYPE=58, VAR=59, MODULE=60, IMPORT=61, STRING=62, HEXDIGIT=63, IDENT=64, 
		LETTER=65, DIGIT=66, COMMENT=67, WS=68;
	public static final int
		RULE_ident = 0, RULE_qualident = 1, RULE_identdef = 2, RULE_integer = 3, 
		RULE_real = 4, RULE_scaleFactor = 5, RULE_number = 6, RULE_constDeclaration = 7, 
		RULE_constExpression = 8, RULE_typeDeclaration = 9, RULE_type = 10, RULE_arrayType = 11, 
		RULE_length = 12, RULE_recordType = 13, RULE_baseType = 14, RULE_fieldListSequence = 15, 
		RULE_fieldList = 16, RULE_identList = 17, RULE_pointerType = 18, RULE_procedureType = 19, 
		RULE_variableDeclaration = 20, RULE_expression = 21, RULE_relation = 22, 
		RULE_simpleExpression = 23, RULE_addOperator = 24, RULE_term = 25, RULE_mulOperator = 26, 
		RULE_factor = 27, RULE_designator = 28, RULE_selector = 29, RULE_set = 30, 
		RULE_element = 31, RULE_expList = 32, RULE_actualParameters = 33, RULE_statement = 34, 
		RULE_assignment = 35, RULE_procedureCall = 36, RULE_statementSequence = 37, 
		RULE_ifStatement = 38, RULE_caseStatement = 39, RULE_case_ = 40, RULE_caseLabelList = 41, 
		RULE_labelRange = 42, RULE_label = 43, RULE_whileStatement = 44, RULE_repeatStatement = 45, 
		RULE_forStatement = 46, RULE_procedureDeclaration = 47, RULE_procedureHeading = 48, 
		RULE_procedureBody = 49, RULE_declarationSequence = 50, RULE_formalParameters = 51, 
		RULE_fPSection = 52, RULE_formalType = 53, RULE_module = 54, RULE_importList = 55, 
		RULE_import_ = 56;
	public static final String[] ruleNames = {
		"ident", "qualident", "identdef", "integer", "real", "scaleFactor", "number", 
		"constDeclaration", "constExpression", "typeDeclaration", "type", "arrayType", 
		"length", "recordType", "baseType", "fieldListSequence", "fieldList", 
		"identList", "pointerType", "procedureType", "variableDeclaration", "expression", 
		"relation", "simpleExpression", "addOperator", "term", "mulOperator", 
		"factor", "designator", "selector", "set", "element", "expList", "actualParameters", 
		"statement", "assignment", "procedureCall", "statementSequence", "ifStatement", 
		"caseStatement", "case_", "caseLabelList", "labelRange", "label", "whileStatement", 
		"repeatStatement", "forStatement", "procedureDeclaration", "procedureHeading", 
		"procedureBody", "declarationSequence", "formalParameters", "fPSection", 
		"formalType", "module", "importList", "import_"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'.'", "'*'", "'H'", "'E'", "'+'", "'-'", "'='", "','", "'('", "')'", 
		"';'", "':'", "'#'", "'<'", "'<='", "'>'", "'>='", "'/'", "'&'", "'~'", 
		"'['", "']'", "'^'", "'{'", "'}'", "'..'", "':='", "'|'", "'ARRAY'", "'OF'", 
		"'END'", "'POINTER'", "'TO'", "'RECORD'", "'PROCEDURE'", "'IN'", "'IS'", 
		"'OR'", "'DIV'", "'MOD'", "'NIL'", "'TRUE'", "'FALSE'", "'IF'", "'THEN'", 
		"'ELSIF'", "'ELSE'", "'CASE'", "'WHILE'", "'DO'", "'REPEAT'", "'UNTIL'", 
		"'FOR'", "'BY'", "'BEGIN'", "'RETURN'", "'CONST'", "'TYPE'", "'VAR'", 
		"'MODULE'", "'IMPORT'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, "ARRAY", "OF", "END", "POINTER", "TO", "RECORD", 
		"PROCEDURE", "IN", "IS", "OR", "DIV", "MOD", "NIL", "TRUE", "FALSE", "IF", 
		"THEN", "ELSIF", "ELSE", "CASE", "WHILE", "DO", "REPEAT", "UNTIL", "FOR", 
		"BY", "BEGIN", "RETURN", "CONST", "TYPE", "VAR", "MODULE", "IMPORT", "STRING", 
		"HEXDIGIT", "IDENT", "LETTER", "DIGIT", "COMMENT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "oberon.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public oberonParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class IdentContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(oberonParser.IDENT, 0); }
		public IdentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ident; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterIdent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitIdent(this);
		}
	}

	public final IdentContext ident() throws RecognitionException {
		IdentContext _localctx = new IdentContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_ident);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(114);
			match(IDENT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class QualidentContext extends ParserRuleContext {
		public List<IdentContext> ident() {
			return getRuleContexts(IdentContext.class);
		}
		public IdentContext ident(int i) {
			return getRuleContext(IdentContext.class,i);
		}
		public QualidentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_qualident; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterQualident(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitQualident(this);
		}
	}

	public final QualidentContext qualident() throws RecognitionException {
		QualidentContext _localctx = new QualidentContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_qualident);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(119);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				setState(116);
				ident();
				setState(117);
				match(T__0);
				}
				break;
			}
			setState(121);
			ident();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentdefContext extends ParserRuleContext {
		public IdentContext ident() {
			return getRuleContext(IdentContext.class,0);
		}
		public IdentdefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identdef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterIdentdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitIdentdef(this);
		}
	}

	public final IdentdefContext identdef() throws RecognitionException {
		IdentdefContext _localctx = new IdentdefContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_identdef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(123);
			ident();
			setState(125);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(124);
				match(T__1);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntegerContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(oberonParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(oberonParser.DIGIT, i);
		}
		public List<TerminalNode> HEXDIGIT() { return getTokens(oberonParser.HEXDIGIT); }
		public TerminalNode HEXDIGIT(int i) {
			return getToken(oberonParser.HEXDIGIT, i);
		}
		public IntegerContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_integer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterInteger(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitInteger(this);
		}
	}

	public final IntegerContext integer() throws RecognitionException {
		IntegerContext _localctx = new IntegerContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_integer);
		int _la;
		try {
			setState(140);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				{
				setState(128); 
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
					{
					setState(127);
					match(DIGIT);
					}
					}
					setState(130); 
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ( _la==DIGIT );
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				{
				setState(132);
				match(DIGIT);
				setState(136);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==HEXDIGIT) {
					{
					{
					setState(133);
					match(HEXDIGIT);
					}
					}
					setState(138);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(139);
				match(T__2);
				}
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RealContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(oberonParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(oberonParser.DIGIT, i);
		}
		public ScaleFactorContext scaleFactor() {
			return getRuleContext(ScaleFactorContext.class,0);
		}
		public RealContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_real; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterReal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitReal(this);
		}
	}

	public final RealContext real() throws RecognitionException {
		RealContext _localctx = new RealContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_real);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(143); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(142);
				match(DIGIT);
				}
				}
				setState(145); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			setState(147);
			match(T__0);
			setState(151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==DIGIT) {
				{
				{
				setState(148);
				match(DIGIT);
				}
				}
				setState(153);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(155);
			_la = _input.LA(1);
			if (_la==T__3) {
				{
				setState(154);
				scaleFactor();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ScaleFactorContext extends ParserRuleContext {
		public List<TerminalNode> DIGIT() { return getTokens(oberonParser.DIGIT); }
		public TerminalNode DIGIT(int i) {
			return getToken(oberonParser.DIGIT, i);
		}
		public ScaleFactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_scaleFactor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterScaleFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitScaleFactor(this);
		}
	}

	public final ScaleFactorContext scaleFactor() throws RecognitionException {
		ScaleFactorContext _localctx = new ScaleFactorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_scaleFactor);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(157);
			match(T__3);
			setState(159);
			_la = _input.LA(1);
			if (_la==T__4 || _la==T__5) {
				{
				setState(158);
				_la = _input.LA(1);
				if ( !(_la==T__4 || _la==T__5) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
			}

			setState(162); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(161);
				match(DIGIT);
				}
				}
				setState(164); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==DIGIT );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NumberContext extends ParserRuleContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public RealContext real() {
			return getRuleContext(RealContext.class,0);
		}
		public NumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_number; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitNumber(this);
		}
	}

	public final NumberContext number() throws RecognitionException {
		NumberContext _localctx = new NumberContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_number);
		try {
			setState(168);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(166);
				integer();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(167);
				real();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstDeclarationContext extends ParserRuleContext {
		public IdentdefContext identdef() {
			return getRuleContext(IdentdefContext.class,0);
		}
		public ConstExpressionContext constExpression() {
			return getRuleContext(ConstExpressionContext.class,0);
		}
		public ConstDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterConstDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitConstDeclaration(this);
		}
	}

	public final ConstDeclarationContext constDeclaration() throws RecognitionException {
		ConstDeclarationContext _localctx = new ConstDeclarationContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_constDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(170);
			identdef();
			setState(171);
			match(T__6);
			setState(172);
			constExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConstExpressionContext extends ParserRuleContext {
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ConstExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterConstExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitConstExpression(this);
		}
	}

	public final ConstExpressionContext constExpression() throws RecognitionException {
		ConstExpressionContext _localctx = new ConstExpressionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_constExpression);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(174);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeDeclarationContext extends ParserRuleContext {
		public IdentdefContext identdef() {
			return getRuleContext(IdentdefContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public TypeDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_typeDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterTypeDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitTypeDeclaration(this);
		}
	}

	public final TypeDeclarationContext typeDeclaration() throws RecognitionException {
		TypeDeclarationContext _localctx = new TypeDeclarationContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_typeDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(176);
			identdef();
			setState(177);
			match(T__6);
			setState(178);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TypeContext extends ParserRuleContext {
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public ArrayTypeContext arrayType() {
			return getRuleContext(ArrayTypeContext.class,0);
		}
		public RecordTypeContext recordType() {
			return getRuleContext(RecordTypeContext.class,0);
		}
		public PointerTypeContext pointerType() {
			return getRuleContext(PointerTypeContext.class,0);
		}
		public ProcedureTypeContext procedureType() {
			return getRuleContext(ProcedureTypeContext.class,0);
		}
		public TypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_type; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitType(this);
		}
	}

	public final TypeContext type() throws RecognitionException {
		TypeContext _localctx = new TypeContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_type);
		try {
			setState(185);
			switch (_input.LA(1)) {
			case IDENT:
				enterOuterAlt(_localctx, 1);
				{
				setState(180);
				qualident();
				}
				break;
			case ARRAY:
				enterOuterAlt(_localctx, 2);
				{
				setState(181);
				arrayType();
				}
				break;
			case RECORD:
				enterOuterAlt(_localctx, 3);
				{
				setState(182);
				recordType();
				}
				break;
			case POINTER:
				enterOuterAlt(_localctx, 4);
				{
				setState(183);
				pointerType();
				}
				break;
			case PROCEDURE:
				enterOuterAlt(_localctx, 5);
				{
				setState(184);
				procedureType();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ArrayTypeContext extends ParserRuleContext {
		public TerminalNode ARRAY() { return getToken(oberonParser.ARRAY, 0); }
		public List<LengthContext> length() {
			return getRuleContexts(LengthContext.class);
		}
		public LengthContext length(int i) {
			return getRuleContext(LengthContext.class,i);
		}
		public TerminalNode OF() { return getToken(oberonParser.OF, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public ArrayTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arrayType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterArrayType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitArrayType(this);
		}
	}

	public final ArrayTypeContext arrayType() throws RecognitionException {
		ArrayTypeContext _localctx = new ArrayTypeContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_arrayType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			match(ARRAY);
			setState(188);
			length();
			setState(193);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(189);
				match(T__7);
				setState(190);
				length();
				}
				}
				setState(195);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(196);
			match(OF);
			setState(197);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LengthContext extends ParserRuleContext {
		public ConstExpressionContext constExpression() {
			return getRuleContext(ConstExpressionContext.class,0);
		}
		public LengthContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_length; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterLength(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitLength(this);
		}
	}

	public final LengthContext length() throws RecognitionException {
		LengthContext _localctx = new LengthContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_length);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(199);
			constExpression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RecordTypeContext extends ParserRuleContext {
		public TerminalNode RECORD() { return getToken(oberonParser.RECORD, 0); }
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public BaseTypeContext baseType() {
			return getRuleContext(BaseTypeContext.class,0);
		}
		public FieldListSequenceContext fieldListSequence() {
			return getRuleContext(FieldListSequenceContext.class,0);
		}
		public RecordTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_recordType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterRecordType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitRecordType(this);
		}
	}

	public final RecordTypeContext recordType() throws RecognitionException {
		RecordTypeContext _localctx = new RecordTypeContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_recordType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(201);
			match(RECORD);
			setState(206);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(202);
				match(T__8);
				setState(203);
				baseType();
				setState(204);
				match(T__9);
				}
			}

			setState(209);
			_la = _input.LA(1);
			if (_la==IDENT) {
				{
				setState(208);
				fieldListSequence();
				}
			}

			setState(211);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BaseTypeContext extends ParserRuleContext {
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public BaseTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_baseType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterBaseType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitBaseType(this);
		}
	}

	public final BaseTypeContext baseType() throws RecognitionException {
		BaseTypeContext _localctx = new BaseTypeContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_baseType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(213);
			qualident();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldListSequenceContext extends ParserRuleContext {
		public List<FieldListContext> fieldList() {
			return getRuleContexts(FieldListContext.class);
		}
		public FieldListContext fieldList(int i) {
			return getRuleContext(FieldListContext.class,i);
		}
		public FieldListSequenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldListSequence; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFieldListSequence(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFieldListSequence(this);
		}
	}

	public final FieldListSequenceContext fieldListSequence() throws RecognitionException {
		FieldListSequenceContext _localctx = new FieldListSequenceContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_fieldListSequence);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(215);
			fieldList();
			setState(220);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(216);
				match(T__10);
				setState(217);
				fieldList();
				}
				}
				setState(222);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FieldListContext extends ParserRuleContext {
		public IdentListContext identList() {
			return getRuleContext(IdentListContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public FieldListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fieldList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFieldList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFieldList(this);
		}
	}

	public final FieldListContext fieldList() throws RecognitionException {
		FieldListContext _localctx = new FieldListContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_fieldList);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(223);
			identList();
			setState(224);
			match(T__11);
			setState(225);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IdentListContext extends ParserRuleContext {
		public List<IdentdefContext> identdef() {
			return getRuleContexts(IdentdefContext.class);
		}
		public IdentdefContext identdef(int i) {
			return getRuleContext(IdentdefContext.class,i);
		}
		public IdentListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_identList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterIdentList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitIdentList(this);
		}
	}

	public final IdentListContext identList() throws RecognitionException {
		IdentListContext _localctx = new IdentListContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_identList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(227);
			identdef();
			setState(232);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(228);
				match(T__7);
				setState(229);
				identdef();
				}
				}
				setState(234);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PointerTypeContext extends ParserRuleContext {
		public TerminalNode POINTER() { return getToken(oberonParser.POINTER, 0); }
		public TerminalNode TO() { return getToken(oberonParser.TO, 0); }
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public PointerTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_pointerType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterPointerType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitPointerType(this);
		}
	}

	public final PointerTypeContext pointerType() throws RecognitionException {
		PointerTypeContext _localctx = new PointerTypeContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_pointerType);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(235);
			match(POINTER);
			setState(236);
			match(TO);
			setState(237);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcedureTypeContext extends ParserRuleContext {
		public TerminalNode PROCEDURE() { return getToken(oberonParser.PROCEDURE, 0); }
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public ProcedureTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterProcedureType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitProcedureType(this);
		}
	}

	public final ProcedureTypeContext procedureType() throws RecognitionException {
		ProcedureTypeContext _localctx = new ProcedureTypeContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_procedureType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(239);
			match(PROCEDURE);
			setState(241);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(240);
				formalParameters();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableDeclarationContext extends ParserRuleContext {
		public IdentListContext identList() {
			return getRuleContext(IdentListContext.class,0);
		}
		public TypeContext type() {
			return getRuleContext(TypeContext.class,0);
		}
		public VariableDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variableDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterVariableDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitVariableDeclaration(this);
		}
	}

	public final VariableDeclarationContext variableDeclaration() throws RecognitionException {
		VariableDeclarationContext _localctx = new VariableDeclarationContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_variableDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(243);
			identList();
			setState(244);
			match(T__11);
			setState(245);
			type();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpressionContext extends ParserRuleContext {
		public List<SimpleExpressionContext> simpleExpression() {
			return getRuleContexts(SimpleExpressionContext.class);
		}
		public SimpleExpressionContext simpleExpression(int i) {
			return getRuleContext(SimpleExpressionContext.class,i);
		}
		public RelationContext relation() {
			return getRuleContext(RelationContext.class,0);
		}
		public ExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitExpression(this);
		}
	}

	public final ExpressionContext expression() throws RecognitionException {
		ExpressionContext _localctx = new ExpressionContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_expression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(247);
			simpleExpression();
			setState(251);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IN) | (1L << IS))) != 0)) {
				{
				setState(248);
				relation();
				setState(249);
				simpleExpression();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RelationContext extends ParserRuleContext {
		public TerminalNode IN() { return getToken(oberonParser.IN, 0); }
		public TerminalNode IS() { return getToken(oberonParser.IS, 0); }
		public RelationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterRelation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitRelation(this);
		}
	}

	public final RelationContext relation() throws RecognitionException {
		RelationContext _localctx = new RelationContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_relation);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(253);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__6) | (1L << T__12) | (1L << T__13) | (1L << T__14) | (1L << T__15) | (1L << T__16) | (1L << IN) | (1L << IS))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SimpleExpressionContext extends ParserRuleContext {
		public List<TermContext> term() {
			return getRuleContexts(TermContext.class);
		}
		public TermContext term(int i) {
			return getRuleContext(TermContext.class,i);
		}
		public List<AddOperatorContext> addOperator() {
			return getRuleContexts(AddOperatorContext.class);
		}
		public AddOperatorContext addOperator(int i) {
			return getRuleContext(AddOperatorContext.class,i);
		}
		public SimpleExpressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_simpleExpression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterSimpleExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitSimpleExpression(this);
		}
	}

	public final SimpleExpressionContext simpleExpression() throws RecognitionException {
		SimpleExpressionContext _localctx = new SimpleExpressionContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_simpleExpression);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(256);
			_la = _input.LA(1);
			if (_la==T__4 || _la==T__5) {
				{
				setState(255);
				_la = _input.LA(1);
				if ( !(_la==T__4 || _la==T__5) ) {
				_errHandler.recoverInline(this);
				} else {
					consume();
				}
				}
			}

			setState(258);
			term();
			setState(264);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << OR))) != 0)) {
				{
				{
				setState(259);
				addOperator();
				setState(260);
				term();
				}
				}
				setState(266);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AddOperatorContext extends ParserRuleContext {
		public TerminalNode OR() { return getToken(oberonParser.OR, 0); }
		public AddOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_addOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterAddOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitAddOperator(this);
		}
	}

	public final AddOperatorContext addOperator() throws RecognitionException {
		AddOperatorContext _localctx = new AddOperatorContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_addOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(267);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__4) | (1L << T__5) | (1L << OR))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TermContext extends ParserRuleContext {
		public List<FactorContext> factor() {
			return getRuleContexts(FactorContext.class);
		}
		public FactorContext factor(int i) {
			return getRuleContext(FactorContext.class,i);
		}
		public List<MulOperatorContext> mulOperator() {
			return getRuleContexts(MulOperatorContext.class);
		}
		public MulOperatorContext mulOperator(int i) {
			return getRuleContext(MulOperatorContext.class,i);
		}
		public TermContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_term; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterTerm(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitTerm(this);
		}
	}

	public final TermContext term() throws RecognitionException {
		TermContext _localctx = new TermContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_term);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(269);
			factor();
			setState(275);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__17) | (1L << T__18) | (1L << DIV) | (1L << MOD))) != 0)) {
				{
				{
				setState(270);
				mulOperator();
				setState(271);
				factor();
				}
				}
				setState(277);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MulOperatorContext extends ParserRuleContext {
		public TerminalNode DIV() { return getToken(oberonParser.DIV, 0); }
		public TerminalNode MOD() { return getToken(oberonParser.MOD, 0); }
		public MulOperatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_mulOperator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterMulOperator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitMulOperator(this);
		}
	}

	public final MulOperatorContext mulOperator() throws RecognitionException {
		MulOperatorContext _localctx = new MulOperatorContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_mulOperator);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(278);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__1) | (1L << T__17) | (1L << T__18) | (1L << DIV) | (1L << MOD))) != 0)) ) {
			_errHandler.recoverInline(this);
			} else {
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FactorContext extends ParserRuleContext {
		public NumberContext number() {
			return getRuleContext(NumberContext.class,0);
		}
		public TerminalNode STRING() { return getToken(oberonParser.STRING, 0); }
		public TerminalNode NIL() { return getToken(oberonParser.NIL, 0); }
		public TerminalNode TRUE() { return getToken(oberonParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(oberonParser.FALSE, 0); }
		public SetContext set() {
			return getRuleContext(SetContext.class,0);
		}
		public DesignatorContext designator() {
			return getRuleContext(DesignatorContext.class,0);
		}
		public ActualParametersContext actualParameters() {
			return getRuleContext(ActualParametersContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public FactorContext factor() {
			return getRuleContext(FactorContext.class,0);
		}
		public FactorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_factor; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFactor(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFactor(this);
		}
	}

	public final FactorContext factor() throws RecognitionException {
		FactorContext _localctx = new FactorContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_factor);
		int _la;
		try {
			setState(296);
			switch (_input.LA(1)) {
			case DIGIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(280);
				number();
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(281);
				match(STRING);
				}
				break;
			case NIL:
				enterOuterAlt(_localctx, 3);
				{
				setState(282);
				match(NIL);
				}
				break;
			case TRUE:
				enterOuterAlt(_localctx, 4);
				{
				setState(283);
				match(TRUE);
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 5);
				{
				setState(284);
				match(FALSE);
				}
				break;
			case T__23:
				enterOuterAlt(_localctx, 6);
				{
				setState(285);
				set();
				}
				break;
			case IDENT:
				enterOuterAlt(_localctx, 7);
				{
				setState(286);
				designator();
				setState(288);
				_la = _input.LA(1);
				if (_la==T__8) {
					{
					setState(287);
					actualParameters();
					}
				}

				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 8);
				{
				setState(290);
				match(T__8);
				setState(291);
				expression();
				setState(292);
				match(T__9);
				}
				break;
			case T__19:
				enterOuterAlt(_localctx, 9);
				{
				setState(294);
				match(T__19);
				setState(295);
				factor();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DesignatorContext extends ParserRuleContext {
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public List<SelectorContext> selector() {
			return getRuleContexts(SelectorContext.class);
		}
		public SelectorContext selector(int i) {
			return getRuleContext(SelectorContext.class,i);
		}
		public DesignatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_designator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterDesignator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitDesignator(this);
		}
	}

	public final DesignatorContext designator() throws RecognitionException {
		DesignatorContext _localctx = new DesignatorContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_designator);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(298);
			qualident();
			setState(302);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(299);
					selector();
					}
					} 
				}
				setState(304);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SelectorContext extends ParserRuleContext {
		public IdentContext ident() {
			return getRuleContext(IdentContext.class,0);
		}
		public ExpListContext expList() {
			return getRuleContext(ExpListContext.class,0);
		}
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public SelectorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_selector; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterSelector(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitSelector(this);
		}
	}

	public final SelectorContext selector() throws RecognitionException {
		SelectorContext _localctx = new SelectorContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_selector);
		try {
			setState(316);
			switch (_input.LA(1)) {
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(305);
				match(T__0);
				setState(306);
				ident();
				}
				break;
			case T__20:
				enterOuterAlt(_localctx, 2);
				{
				setState(307);
				match(T__20);
				setState(308);
				expList();
				setState(309);
				match(T__21);
				}
				break;
			case T__22:
				enterOuterAlt(_localctx, 3);
				{
				setState(311);
				match(T__22);
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 4);
				{
				setState(312);
				match(T__8);
				setState(313);
				qualident();
				setState(314);
				match(T__9);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SetContext extends ParserRuleContext {
		public List<ElementContext> element() {
			return getRuleContexts(ElementContext.class);
		}
		public ElementContext element(int i) {
			return getRuleContext(ElementContext.class,i);
		}
		public SetContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_set; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterSet(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitSet(this);
		}
	}

	public final SetContext set() throws RecognitionException {
		SetContext _localctx = new SetContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_set);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(318);
			match(T__23);
			setState(327);
			_la = _input.LA(1);
			if (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__5 - 5)) | (1L << (T__8 - 5)) | (1L << (T__19 - 5)) | (1L << (T__23 - 5)) | (1L << (NIL - 5)) | (1L << (TRUE - 5)) | (1L << (FALSE - 5)) | (1L << (STRING - 5)) | (1L << (IDENT - 5)) | (1L << (DIGIT - 5)))) != 0)) {
				{
				setState(319);
				element();
				setState(324);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__7) {
					{
					{
					setState(320);
					match(T__7);
					setState(321);
					element();
					}
					}
					setState(326);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(329);
			match(T__24);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ElementContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ElementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_element; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterElement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitElement(this);
		}
	}

	public final ElementContext element() throws RecognitionException {
		ElementContext _localctx = new ElementContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_element);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(331);
			expression();
			setState(334);
			_la = _input.LA(1);
			if (_la==T__25) {
				{
				setState(332);
				match(T__25);
				setState(333);
				expression();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpListContext extends ParserRuleContext {
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public ExpListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterExpList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitExpList(this);
		}
	}

	public final ExpListContext expList() throws RecognitionException {
		ExpListContext _localctx = new ExpListContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_expList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(336);
			expression();
			setState(341);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(337);
				match(T__7);
				setState(338);
				expression();
				}
				}
				setState(343);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ActualParametersContext extends ParserRuleContext {
		public ExpListContext expList() {
			return getRuleContext(ExpListContext.class,0);
		}
		public ActualParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_actualParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterActualParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitActualParameters(this);
		}
	}

	public final ActualParametersContext actualParameters() throws RecognitionException {
		ActualParametersContext _localctx = new ActualParametersContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_actualParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(344);
			match(T__8);
			setState(346);
			_la = _input.LA(1);
			if (((((_la - 5)) & ~0x3f) == 0 && ((1L << (_la - 5)) & ((1L << (T__4 - 5)) | (1L << (T__5 - 5)) | (1L << (T__8 - 5)) | (1L << (T__19 - 5)) | (1L << (T__23 - 5)) | (1L << (NIL - 5)) | (1L << (TRUE - 5)) | (1L << (FALSE - 5)) | (1L << (STRING - 5)) | (1L << (IDENT - 5)) | (1L << (DIGIT - 5)))) != 0)) {
				{
				setState(345);
				expList();
				}
			}

			setState(348);
			match(T__9);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementContext extends ParserRuleContext {
		public AssignmentContext assignment() {
			return getRuleContext(AssignmentContext.class,0);
		}
		public ProcedureCallContext procedureCall() {
			return getRuleContext(ProcedureCallContext.class,0);
		}
		public IfStatementContext ifStatement() {
			return getRuleContext(IfStatementContext.class,0);
		}
		public CaseStatementContext caseStatement() {
			return getRuleContext(CaseStatementContext.class,0);
		}
		public WhileStatementContext whileStatement() {
			return getRuleContext(WhileStatementContext.class,0);
		}
		public RepeatStatementContext repeatStatement() {
			return getRuleContext(RepeatStatementContext.class,0);
		}
		public ForStatementContext forStatement() {
			return getRuleContext(ForStatementContext.class,0);
		}
		public StatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitStatement(this);
		}
	}

	public final StatementContext statement() throws RecognitionException {
		StatementContext _localctx = new StatementContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_statement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(357);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,31,_ctx) ) {
			case 1:
				{
				setState(350);
				assignment();
				}
				break;
			case 2:
				{
				setState(351);
				procedureCall();
				}
				break;
			case 3:
				{
				setState(352);
				ifStatement();
				}
				break;
			case 4:
				{
				setState(353);
				caseStatement();
				}
				break;
			case 5:
				{
				setState(354);
				whileStatement();
				}
				break;
			case 6:
				{
				setState(355);
				repeatStatement();
				}
				break;
			case 7:
				{
				setState(356);
				forStatement();
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AssignmentContext extends ParserRuleContext {
		public DesignatorContext designator() {
			return getRuleContext(DesignatorContext.class,0);
		}
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public AssignmentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_assignment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterAssignment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitAssignment(this);
		}
	}

	public final AssignmentContext assignment() throws RecognitionException {
		AssignmentContext _localctx = new AssignmentContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_assignment);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(359);
			designator();
			setState(360);
			match(T__26);
			setState(361);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcedureCallContext extends ParserRuleContext {
		public DesignatorContext designator() {
			return getRuleContext(DesignatorContext.class,0);
		}
		public ActualParametersContext actualParameters() {
			return getRuleContext(ActualParametersContext.class,0);
		}
		public ProcedureCallContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureCall; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterProcedureCall(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitProcedureCall(this);
		}
	}

	public final ProcedureCallContext procedureCall() throws RecognitionException {
		ProcedureCallContext _localctx = new ProcedureCallContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_procedureCall);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(363);
			designator();
			setState(365);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(364);
				actualParameters();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatementSequenceContext extends ParserRuleContext {
		public List<StatementContext> statement() {
			return getRuleContexts(StatementContext.class);
		}
		public StatementContext statement(int i) {
			return getRuleContext(StatementContext.class,i);
		}
		public StatementSequenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_statementSequence; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterStatementSequence(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitStatementSequence(this);
		}
	}

	public final StatementSequenceContext statementSequence() throws RecognitionException {
		StatementSequenceContext _localctx = new StatementSequenceContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_statementSequence);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(367);
			statement();
			setState(372);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__10) {
				{
				{
				setState(368);
				match(T__10);
				setState(369);
				statement();
				}
				}
				setState(374);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfStatementContext extends ParserRuleContext {
		public TerminalNode IF() { return getToken(oberonParser.IF, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> THEN() { return getTokens(oberonParser.THEN); }
		public TerminalNode THEN(int i) {
			return getToken(oberonParser.THEN, i);
		}
		public List<StatementSequenceContext> statementSequence() {
			return getRuleContexts(StatementSequenceContext.class);
		}
		public StatementSequenceContext statementSequence(int i) {
			return getRuleContext(StatementSequenceContext.class,i);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public List<TerminalNode> ELSIF() { return getTokens(oberonParser.ELSIF); }
		public TerminalNode ELSIF(int i) {
			return getToken(oberonParser.ELSIF, i);
		}
		public TerminalNode ELSE() { return getToken(oberonParser.ELSE, 0); }
		public IfStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterIfStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitIfStatement(this);
		}
	}

	public final IfStatementContext ifStatement() throws RecognitionException {
		IfStatementContext _localctx = new IfStatementContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_ifStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(375);
			match(IF);
			setState(376);
			expression();
			setState(377);
			match(THEN);
			setState(378);
			statementSequence();
			setState(386);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELSIF) {
				{
				{
				setState(379);
				match(ELSIF);
				setState(380);
				expression();
				setState(381);
				match(THEN);
				setState(382);
				statementSequence();
				}
				}
				setState(388);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(391);
			_la = _input.LA(1);
			if (_la==ELSE) {
				{
				setState(389);
				match(ELSE);
				setState(390);
				statementSequence();
				}
			}

			setState(393);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseStatementContext extends ParserRuleContext {
		public TerminalNode CASE() { return getToken(oberonParser.CASE, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public TerminalNode OF() { return getToken(oberonParser.OF, 0); }
		public List<Case_Context> case_() {
			return getRuleContexts(Case_Context.class);
		}
		public Case_Context case_(int i) {
			return getRuleContext(Case_Context.class,i);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public CaseStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterCaseStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitCaseStatement(this);
		}
	}

	public final CaseStatementContext caseStatement() throws RecognitionException {
		CaseStatementContext _localctx = new CaseStatementContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_caseStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(395);
			match(CASE);
			setState(396);
			expression();
			setState(397);
			match(OF);
			setState(398);
			case_();
			setState(403);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__27) {
				{
				{
				setState(399);
				match(T__27);
				setState(400);
				case_();
				}
				}
				setState(405);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(406);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Case_Context extends ParserRuleContext {
		public CaseLabelListContext caseLabelList() {
			return getRuleContext(CaseLabelListContext.class,0);
		}
		public StatementSequenceContext statementSequence() {
			return getRuleContext(StatementSequenceContext.class,0);
		}
		public Case_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_case_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterCase_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitCase_(this);
		}
	}

	public final Case_Context case_() throws RecognitionException {
		Case_Context _localctx = new Case_Context(_ctx, getState());
		enterRule(_localctx, 80, RULE_case_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(412);
			_la = _input.LA(1);
			if (((((_la - 62)) & ~0x3f) == 0 && ((1L << (_la - 62)) & ((1L << (STRING - 62)) | (1L << (IDENT - 62)) | (1L << (DIGIT - 62)))) != 0)) {
				{
				setState(408);
				caseLabelList();
				setState(409);
				match(T__11);
				setState(410);
				statementSequence();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class CaseLabelListContext extends ParserRuleContext {
		public List<LabelRangeContext> labelRange() {
			return getRuleContexts(LabelRangeContext.class);
		}
		public LabelRangeContext labelRange(int i) {
			return getRuleContext(LabelRangeContext.class,i);
		}
		public CaseLabelListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_caseLabelList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterCaseLabelList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitCaseLabelList(this);
		}
	}

	public final CaseLabelListContext caseLabelList() throws RecognitionException {
		CaseLabelListContext _localctx = new CaseLabelListContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_caseLabelList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(414);
			labelRange();
			setState(419);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(415);
				match(T__7);
				setState(416);
				labelRange();
				}
				}
				setState(421);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelRangeContext extends ParserRuleContext {
		public List<LabelContext> label() {
			return getRuleContexts(LabelContext.class);
		}
		public LabelContext label(int i) {
			return getRuleContext(LabelContext.class,i);
		}
		public LabelRangeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_labelRange; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterLabelRange(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitLabelRange(this);
		}
	}

	public final LabelRangeContext labelRange() throws RecognitionException {
		LabelRangeContext _localctx = new LabelRangeContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_labelRange);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(422);
			label();
			setState(425);
			_la = _input.LA(1);
			if (_la==T__25) {
				{
				setState(423);
				match(T__25);
				setState(424);
				label();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LabelContext extends ParserRuleContext {
		public IntegerContext integer() {
			return getRuleContext(IntegerContext.class,0);
		}
		public TerminalNode STRING() { return getToken(oberonParser.STRING, 0); }
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public LabelContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_label; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterLabel(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitLabel(this);
		}
	}

	public final LabelContext label() throws RecognitionException {
		LabelContext _localctx = new LabelContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_label);
		try {
			setState(430);
			switch (_input.LA(1)) {
			case DIGIT:
				enterOuterAlt(_localctx, 1);
				{
				setState(427);
				integer();
				}
				break;
			case STRING:
				enterOuterAlt(_localctx, 2);
				{
				setState(428);
				match(STRING);
				}
				break;
			case IDENT:
				enterOuterAlt(_localctx, 3);
				{
				setState(429);
				qualident();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WhileStatementContext extends ParserRuleContext {
		public TerminalNode WHILE() { return getToken(oberonParser.WHILE, 0); }
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public List<TerminalNode> DO() { return getTokens(oberonParser.DO); }
		public TerminalNode DO(int i) {
			return getToken(oberonParser.DO, i);
		}
		public List<StatementSequenceContext> statementSequence() {
			return getRuleContexts(StatementSequenceContext.class);
		}
		public StatementSequenceContext statementSequence(int i) {
			return getRuleContext(StatementSequenceContext.class,i);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public List<TerminalNode> ELSIF() { return getTokens(oberonParser.ELSIF); }
		public TerminalNode ELSIF(int i) {
			return getToken(oberonParser.ELSIF, i);
		}
		public WhileStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_whileStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterWhileStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitWhileStatement(this);
		}
	}

	public final WhileStatementContext whileStatement() throws RecognitionException {
		WhileStatementContext _localctx = new WhileStatementContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_whileStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(432);
			match(WHILE);
			setState(433);
			expression();
			setState(434);
			match(DO);
			setState(435);
			statementSequence();
			setState(443);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ELSIF) {
				{
				{
				setState(436);
				match(ELSIF);
				setState(437);
				expression();
				setState(438);
				match(DO);
				setState(439);
				statementSequence();
				}
				}
				setState(445);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(446);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RepeatStatementContext extends ParserRuleContext {
		public TerminalNode REPEAT() { return getToken(oberonParser.REPEAT, 0); }
		public StatementSequenceContext statementSequence() {
			return getRuleContext(StatementSequenceContext.class,0);
		}
		public TerminalNode UNTIL() { return getToken(oberonParser.UNTIL, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public RepeatStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_repeatStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterRepeatStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitRepeatStatement(this);
		}
	}

	public final RepeatStatementContext repeatStatement() throws RecognitionException {
		RepeatStatementContext _localctx = new RepeatStatementContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_repeatStatement);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(448);
			match(REPEAT);
			setState(449);
			statementSequence();
			setState(450);
			match(UNTIL);
			setState(451);
			expression();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ForStatementContext extends ParserRuleContext {
		public TerminalNode FOR() { return getToken(oberonParser.FOR, 0); }
		public IdentContext ident() {
			return getRuleContext(IdentContext.class,0);
		}
		public List<ExpressionContext> expression() {
			return getRuleContexts(ExpressionContext.class);
		}
		public ExpressionContext expression(int i) {
			return getRuleContext(ExpressionContext.class,i);
		}
		public TerminalNode TO() { return getToken(oberonParser.TO, 0); }
		public TerminalNode DO() { return getToken(oberonParser.DO, 0); }
		public StatementSequenceContext statementSequence() {
			return getRuleContext(StatementSequenceContext.class,0);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public TerminalNode BY() { return getToken(oberonParser.BY, 0); }
		public ConstExpressionContext constExpression() {
			return getRuleContext(ConstExpressionContext.class,0);
		}
		public ForStatementContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_forStatement; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterForStatement(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitForStatement(this);
		}
	}

	public final ForStatementContext forStatement() throws RecognitionException {
		ForStatementContext _localctx = new ForStatementContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_forStatement);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(453);
			match(FOR);
			setState(454);
			ident();
			setState(455);
			match(T__26);
			setState(456);
			expression();
			setState(457);
			match(TO);
			setState(458);
			expression();
			setState(461);
			_la = _input.LA(1);
			if (_la==BY) {
				{
				setState(459);
				match(BY);
				setState(460);
				constExpression();
				}
			}

			setState(463);
			match(DO);
			setState(464);
			statementSequence();
			setState(465);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcedureDeclarationContext extends ParserRuleContext {
		public ProcedureHeadingContext procedureHeading() {
			return getRuleContext(ProcedureHeadingContext.class,0);
		}
		public ProcedureBodyContext procedureBody() {
			return getRuleContext(ProcedureBodyContext.class,0);
		}
		public IdentContext ident() {
			return getRuleContext(IdentContext.class,0);
		}
		public ProcedureDeclarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureDeclaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterProcedureDeclaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitProcedureDeclaration(this);
		}
	}

	public final ProcedureDeclarationContext procedureDeclaration() throws RecognitionException {
		ProcedureDeclarationContext _localctx = new ProcedureDeclarationContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_procedureDeclaration);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(467);
			procedureHeading();
			setState(468);
			match(T__10);
			setState(469);
			procedureBody();
			setState(470);
			ident();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcedureHeadingContext extends ParserRuleContext {
		public TerminalNode PROCEDURE() { return getToken(oberonParser.PROCEDURE, 0); }
		public IdentdefContext identdef() {
			return getRuleContext(IdentdefContext.class,0);
		}
		public FormalParametersContext formalParameters() {
			return getRuleContext(FormalParametersContext.class,0);
		}
		public ProcedureHeadingContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureHeading; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterProcedureHeading(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitProcedureHeading(this);
		}
	}

	public final ProcedureHeadingContext procedureHeading() throws RecognitionException {
		ProcedureHeadingContext _localctx = new ProcedureHeadingContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_procedureHeading);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(472);
			match(PROCEDURE);
			setState(473);
			identdef();
			setState(475);
			_la = _input.LA(1);
			if (_la==T__8) {
				{
				setState(474);
				formalParameters();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ProcedureBodyContext extends ParserRuleContext {
		public DeclarationSequenceContext declarationSequence() {
			return getRuleContext(DeclarationSequenceContext.class,0);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public TerminalNode BEGIN() { return getToken(oberonParser.BEGIN, 0); }
		public StatementSequenceContext statementSequence() {
			return getRuleContext(StatementSequenceContext.class,0);
		}
		public TerminalNode RETURN() { return getToken(oberonParser.RETURN, 0); }
		public ExpressionContext expression() {
			return getRuleContext(ExpressionContext.class,0);
		}
		public ProcedureBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_procedureBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterProcedureBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitProcedureBody(this);
		}
	}

	public final ProcedureBodyContext procedureBody() throws RecognitionException {
		ProcedureBodyContext _localctx = new ProcedureBodyContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_procedureBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(477);
			declarationSequence();
			setState(480);
			_la = _input.LA(1);
			if (_la==BEGIN) {
				{
				setState(478);
				match(BEGIN);
				setState(479);
				statementSequence();
				}
			}

			setState(484);
			_la = _input.LA(1);
			if (_la==RETURN) {
				{
				setState(482);
				match(RETURN);
				setState(483);
				expression();
				}
			}

			setState(486);
			match(END);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DeclarationSequenceContext extends ParserRuleContext {
		public TerminalNode CONST() { return getToken(oberonParser.CONST, 0); }
		public TerminalNode TYPE() { return getToken(oberonParser.TYPE, 0); }
		public TerminalNode VAR() { return getToken(oberonParser.VAR, 0); }
		public List<ProcedureDeclarationContext> procedureDeclaration() {
			return getRuleContexts(ProcedureDeclarationContext.class);
		}
		public ProcedureDeclarationContext procedureDeclaration(int i) {
			return getRuleContext(ProcedureDeclarationContext.class,i);
		}
		public List<ConstDeclarationContext> constDeclaration() {
			return getRuleContexts(ConstDeclarationContext.class);
		}
		public ConstDeclarationContext constDeclaration(int i) {
			return getRuleContext(ConstDeclarationContext.class,i);
		}
		public List<TypeDeclarationContext> typeDeclaration() {
			return getRuleContexts(TypeDeclarationContext.class);
		}
		public TypeDeclarationContext typeDeclaration(int i) {
			return getRuleContext(TypeDeclarationContext.class,i);
		}
		public List<VariableDeclarationContext> variableDeclaration() {
			return getRuleContexts(VariableDeclarationContext.class);
		}
		public VariableDeclarationContext variableDeclaration(int i) {
			return getRuleContext(VariableDeclarationContext.class,i);
		}
		public DeclarationSequenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_declarationSequence; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterDeclarationSequence(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitDeclarationSequence(this);
		}
	}

	public final DeclarationSequenceContext declarationSequence() throws RecognitionException {
		DeclarationSequenceContext _localctx = new DeclarationSequenceContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_declarationSequence);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(497);
			_la = _input.LA(1);
			if (_la==CONST) {
				{
				setState(488);
				match(CONST);
				setState(494);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==IDENT) {
					{
					{
					setState(489);
					constDeclaration();
					setState(490);
					match(T__10);
					}
					}
					setState(496);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(508);
			_la = _input.LA(1);
			if (_la==TYPE) {
				{
				setState(499);
				match(TYPE);
				setState(505);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==IDENT) {
					{
					{
					setState(500);
					typeDeclaration();
					setState(501);
					match(T__10);
					}
					}
					setState(507);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(519);
			_la = _input.LA(1);
			if (_la==VAR) {
				{
				setState(510);
				match(VAR);
				setState(516);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==IDENT) {
					{
					{
					setState(511);
					variableDeclaration();
					setState(512);
					match(T__10);
					}
					}
					setState(518);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(526);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==PROCEDURE) {
				{
				{
				setState(521);
				procedureDeclaration();
				setState(522);
				match(T__10);
				}
				}
				setState(528);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalParametersContext extends ParserRuleContext {
		public List<FPSectionContext> fPSection() {
			return getRuleContexts(FPSectionContext.class);
		}
		public FPSectionContext fPSection(int i) {
			return getRuleContext(FPSectionContext.class,i);
		}
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public FormalParametersContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalParameters; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFormalParameters(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFormalParameters(this);
		}
	}

	public final FormalParametersContext formalParameters() throws RecognitionException {
		FormalParametersContext _localctx = new FormalParametersContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_formalParameters);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(529);
			match(T__8);
			setState(538);
			_la = _input.LA(1);
			if (_la==VAR || _la==IDENT) {
				{
				setState(530);
				fPSection();
				setState(535);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==T__10) {
					{
					{
					setState(531);
					match(T__10);
					setState(532);
					fPSection();
					}
					}
					setState(537);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				}
			}

			setState(540);
			match(T__9);
			setState(543);
			_la = _input.LA(1);
			if (_la==T__11) {
				{
				setState(541);
				match(T__11);
				setState(542);
				qualident();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FPSectionContext extends ParserRuleContext {
		public List<IdentContext> ident() {
			return getRuleContexts(IdentContext.class);
		}
		public IdentContext ident(int i) {
			return getRuleContext(IdentContext.class,i);
		}
		public FormalTypeContext formalType() {
			return getRuleContext(FormalTypeContext.class,0);
		}
		public TerminalNode VAR() { return getToken(oberonParser.VAR, 0); }
		public FPSectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_fPSection; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFPSection(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFPSection(this);
		}
	}

	public final FPSectionContext fPSection() throws RecognitionException {
		FPSectionContext _localctx = new FPSectionContext(_ctx, getState());
		enterRule(_localctx, 104, RULE_fPSection);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(546);
			_la = _input.LA(1);
			if (_la==VAR) {
				{
				setState(545);
				match(VAR);
				}
			}

			setState(548);
			ident();
			setState(553);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(549);
				match(T__7);
				setState(550);
				ident();
				}
				}
				setState(555);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(556);
			match(T__11);
			setState(557);
			formalType();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FormalTypeContext extends ParserRuleContext {
		public QualidentContext qualident() {
			return getRuleContext(QualidentContext.class,0);
		}
		public List<TerminalNode> ARRAY() { return getTokens(oberonParser.ARRAY); }
		public TerminalNode ARRAY(int i) {
			return getToken(oberonParser.ARRAY, i);
		}
		public List<TerminalNode> OF() { return getTokens(oberonParser.OF); }
		public TerminalNode OF(int i) {
			return getToken(oberonParser.OF, i);
		}
		public FormalTypeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_formalType; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterFormalType(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitFormalType(this);
		}
	}

	public final FormalTypeContext formalType() throws RecognitionException {
		FormalTypeContext _localctx = new FormalTypeContext(_ctx, getState());
		enterRule(_localctx, 106, RULE_formalType);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(563);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==ARRAY) {
				{
				{
				setState(559);
				match(ARRAY);
				setState(560);
				match(OF);
				}
				}
				setState(565);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(566);
			qualident();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ModuleContext extends ParserRuleContext {
		public TerminalNode MODULE() { return getToken(oberonParser.MODULE, 0); }
		public List<IdentContext> ident() {
			return getRuleContexts(IdentContext.class);
		}
		public IdentContext ident(int i) {
			return getRuleContext(IdentContext.class,i);
		}
		public DeclarationSequenceContext declarationSequence() {
			return getRuleContext(DeclarationSequenceContext.class,0);
		}
		public TerminalNode END() { return getToken(oberonParser.END, 0); }
		public ImportListContext importList() {
			return getRuleContext(ImportListContext.class,0);
		}
		public TerminalNode BEGIN() { return getToken(oberonParser.BEGIN, 0); }
		public StatementSequenceContext statementSequence() {
			return getRuleContext(StatementSequenceContext.class,0);
		}
		public ModuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_module; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterModule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitModule(this);
		}
	}

	public final ModuleContext module() throws RecognitionException {
		ModuleContext _localctx = new ModuleContext(_ctx, getState());
		enterRule(_localctx, 108, RULE_module);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(568);
			match(MODULE);
			setState(569);
			ident();
			setState(570);
			match(T__10);
			setState(572);
			_la = _input.LA(1);
			if (_la==IMPORT) {
				{
				setState(571);
				importList();
				}
			}

			setState(574);
			declarationSequence();
			setState(577);
			_la = _input.LA(1);
			if (_la==BEGIN) {
				{
				setState(575);
				match(BEGIN);
				setState(576);
				statementSequence();
				}
			}

			setState(579);
			match(END);
			setState(580);
			ident();
			setState(581);
			match(T__0);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ImportListContext extends ParserRuleContext {
		public TerminalNode IMPORT() { return getToken(oberonParser.IMPORT, 0); }
		public List<Import_Context> import_() {
			return getRuleContexts(Import_Context.class);
		}
		public Import_Context import_(int i) {
			return getRuleContext(Import_Context.class,i);
		}
		public ImportListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_importList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterImportList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitImportList(this);
		}
	}

	public final ImportListContext importList() throws RecognitionException {
		ImportListContext _localctx = new ImportListContext(_ctx, getState());
		enterRule(_localctx, 110, RULE_importList);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(583);
			match(IMPORT);
			setState(584);
			import_();
			setState(589);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__7) {
				{
				{
				setState(585);
				match(T__7);
				setState(586);
				import_();
				}
				}
				setState(591);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(592);
			match(T__10);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Import_Context extends ParserRuleContext {
		public List<IdentContext> ident() {
			return getRuleContexts(IdentContext.class);
		}
		public IdentContext ident(int i) {
			return getRuleContext(IdentContext.class,i);
		}
		public Import_Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_import_; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).enterImport_(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof oberonListener ) ((oberonListener)listener).exitImport_(this);
		}
	}

	public final Import_Context import_() throws RecognitionException {
		Import_Context _localctx = new Import_Context(_ctx, getState());
		enterRule(_localctx, 112, RULE_import_);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(594);
			ident();
			setState(597);
			_la = _input.LA(1);
			if (_la==T__26) {
				{
				setState(595);
				match(T__26);
				setState(596);
				ident();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3F\u025a\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!"+
		"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4"+
		",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t"+
		"\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\3\2\3\2\3\3\3\3\3"+
		"\3\5\3z\n\3\3\3\3\3\3\4\3\4\5\4\u0080\n\4\3\5\6\5\u0083\n\5\r\5\16\5\u0084"+
		"\3\5\3\5\7\5\u0089\n\5\f\5\16\5\u008c\13\5\3\5\5\5\u008f\n\5\3\6\6\6\u0092"+
		"\n\6\r\6\16\6\u0093\3\6\3\6\7\6\u0098\n\6\f\6\16\6\u009b\13\6\3\6\5\6"+
		"\u009e\n\6\3\7\3\7\5\7\u00a2\n\7\3\7\6\7\u00a5\n\7\r\7\16\7\u00a6\3\b"+
		"\3\b\5\b\u00ab\n\b\3\t\3\t\3\t\3\t\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f"+
		"\3\f\3\f\3\f\5\f\u00bc\n\f\3\r\3\r\3\r\3\r\7\r\u00c2\n\r\f\r\16\r\u00c5"+
		"\13\r\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\17\3\17\3\17\5\17\u00d1\n\17\3"+
		"\17\5\17\u00d4\n\17\3\17\3\17\3\20\3\20\3\21\3\21\3\21\7\21\u00dd\n\21"+
		"\f\21\16\21\u00e0\13\21\3\22\3\22\3\22\3\22\3\23\3\23\3\23\7\23\u00e9"+
		"\n\23\f\23\16\23\u00ec\13\23\3\24\3\24\3\24\3\24\3\25\3\25\5\25\u00f4"+
		"\n\25\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\5\27\u00fe\n\27\3\30\3\30"+
		"\3\31\5\31\u0103\n\31\3\31\3\31\3\31\3\31\7\31\u0109\n\31\f\31\16\31\u010c"+
		"\13\31\3\32\3\32\3\33\3\33\3\33\3\33\7\33\u0114\n\33\f\33\16\33\u0117"+
		"\13\33\3\34\3\34\3\35\3\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u0123\n"+
		"\35\3\35\3\35\3\35\3\35\3\35\3\35\5\35\u012b\n\35\3\36\3\36\7\36\u012f"+
		"\n\36\f\36\16\36\u0132\13\36\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3\37\3"+
		"\37\3\37\3\37\5\37\u013f\n\37\3 \3 \3 \3 \7 \u0145\n \f \16 \u0148\13"+
		" \5 \u014a\n \3 \3 \3!\3!\3!\5!\u0151\n!\3\"\3\"\3\"\7\"\u0156\n\"\f\""+
		"\16\"\u0159\13\"\3#\3#\5#\u015d\n#\3#\3#\3$\3$\3$\3$\3$\3$\3$\5$\u0168"+
		"\n$\3%\3%\3%\3%\3&\3&\5&\u0170\n&\3\'\3\'\3\'\7\'\u0175\n\'\f\'\16\'\u0178"+
		"\13\'\3(\3(\3(\3(\3(\3(\3(\3(\3(\7(\u0183\n(\f(\16(\u0186\13(\3(\3(\5"+
		"(\u018a\n(\3(\3(\3)\3)\3)\3)\3)\3)\7)\u0194\n)\f)\16)\u0197\13)\3)\3)"+
		"\3*\3*\3*\3*\5*\u019f\n*\3+\3+\3+\7+\u01a4\n+\f+\16+\u01a7\13+\3,\3,\3"+
		",\5,\u01ac\n,\3-\3-\3-\5-\u01b1\n-\3.\3.\3.\3.\3.\3.\3.\3.\3.\7.\u01bc"+
		"\n.\f.\16.\u01bf\13.\3.\3.\3/\3/\3/\3/\3/\3\60\3\60\3\60\3\60\3\60\3\60"+
		"\3\60\3\60\5\60\u01d0\n\60\3\60\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61"+
		"\3\62\3\62\3\62\5\62\u01de\n\62\3\63\3\63\3\63\5\63\u01e3\n\63\3\63\3"+
		"\63\5\63\u01e7\n\63\3\63\3\63\3\64\3\64\3\64\3\64\7\64\u01ef\n\64\f\64"+
		"\16\64\u01f2\13\64\5\64\u01f4\n\64\3\64\3\64\3\64\3\64\7\64\u01fa\n\64"+
		"\f\64\16\64\u01fd\13\64\5\64\u01ff\n\64\3\64\3\64\3\64\3\64\7\64\u0205"+
		"\n\64\f\64\16\64\u0208\13\64\5\64\u020a\n\64\3\64\3\64\3\64\7\64\u020f"+
		"\n\64\f\64\16\64\u0212\13\64\3\65\3\65\3\65\3\65\7\65\u0218\n\65\f\65"+
		"\16\65\u021b\13\65\5\65\u021d\n\65\3\65\3\65\3\65\5\65\u0222\n\65\3\66"+
		"\5\66\u0225\n\66\3\66\3\66\3\66\7\66\u022a\n\66\f\66\16\66\u022d\13\66"+
		"\3\66\3\66\3\66\3\67\3\67\7\67\u0234\n\67\f\67\16\67\u0237\13\67\3\67"+
		"\3\67\38\38\38\38\58\u023f\n8\38\38\38\58\u0244\n8\38\38\38\38\39\39\3"+
		"9\39\79\u024e\n9\f9\169\u0251\139\39\39\3:\3:\3:\5:\u0258\n:\3:\2\2;\2"+
		"\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJL"+
		"NPRTVXZ\\^`bdfhjlnpr\2\6\3\2\7\b\5\2\t\t\17\23&\'\4\2\7\b((\5\2\4\4\24"+
		"\25)*\u0272\2t\3\2\2\2\4y\3\2\2\2\6}\3\2\2\2\b\u008e\3\2\2\2\n\u0091\3"+
		"\2\2\2\f\u009f\3\2\2\2\16\u00aa\3\2\2\2\20\u00ac\3\2\2\2\22\u00b0\3\2"+
		"\2\2\24\u00b2\3\2\2\2\26\u00bb\3\2\2\2\30\u00bd\3\2\2\2\32\u00c9\3\2\2"+
		"\2\34\u00cb\3\2\2\2\36\u00d7\3\2\2\2 \u00d9\3\2\2\2\"\u00e1\3\2\2\2$\u00e5"+
		"\3\2\2\2&\u00ed\3\2\2\2(\u00f1\3\2\2\2*\u00f5\3\2\2\2,\u00f9\3\2\2\2."+
		"\u00ff\3\2\2\2\60\u0102\3\2\2\2\62\u010d\3\2\2\2\64\u010f\3\2\2\2\66\u0118"+
		"\3\2\2\28\u012a\3\2\2\2:\u012c\3\2\2\2<\u013e\3\2\2\2>\u0140\3\2\2\2@"+
		"\u014d\3\2\2\2B\u0152\3\2\2\2D\u015a\3\2\2\2F\u0167\3\2\2\2H\u0169\3\2"+
		"\2\2J\u016d\3\2\2\2L\u0171\3\2\2\2N\u0179\3\2\2\2P\u018d\3\2\2\2R\u019e"+
		"\3\2\2\2T\u01a0\3\2\2\2V\u01a8\3\2\2\2X\u01b0\3\2\2\2Z\u01b2\3\2\2\2\\"+
		"\u01c2\3\2\2\2^\u01c7\3\2\2\2`\u01d5\3\2\2\2b\u01da\3\2\2\2d\u01df\3\2"+
		"\2\2f\u01f3\3\2\2\2h\u0213\3\2\2\2j\u0224\3\2\2\2l\u0235\3\2\2\2n\u023a"+
		"\3\2\2\2p\u0249\3\2\2\2r\u0254\3\2\2\2tu\7B\2\2u\3\3\2\2\2vw\5\2\2\2w"+
		"x\7\3\2\2xz\3\2\2\2yv\3\2\2\2yz\3\2\2\2z{\3\2\2\2{|\5\2\2\2|\5\3\2\2\2"+
		"}\177\5\2\2\2~\u0080\7\4\2\2\177~\3\2\2\2\177\u0080\3\2\2\2\u0080\7\3"+
		"\2\2\2\u0081\u0083\7D\2\2\u0082\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084"+
		"\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085\u008f\3\2\2\2\u0086\u008a\7D"+
		"\2\2\u0087\u0089\7A\2\2\u0088\u0087\3\2\2\2\u0089\u008c\3\2\2\2\u008a"+
		"\u0088\3\2\2\2\u008a\u008b\3\2\2\2\u008b\u008d\3\2\2\2\u008c\u008a\3\2"+
		"\2\2\u008d\u008f\7\5\2\2\u008e\u0082\3\2\2\2\u008e\u0086\3\2\2\2\u008f"+
		"\t\3\2\2\2\u0090\u0092\7D\2\2\u0091\u0090\3\2\2\2\u0092\u0093\3\2\2\2"+
		"\u0093\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0099"+
		"\7\3\2\2\u0096\u0098\7D\2\2\u0097\u0096\3\2\2\2\u0098\u009b\3\2\2\2\u0099"+
		"\u0097\3\2\2\2\u0099\u009a\3\2\2\2\u009a\u009d\3\2\2\2\u009b\u0099\3\2"+
		"\2\2\u009c\u009e\5\f\7\2\u009d\u009c\3\2\2\2\u009d\u009e\3\2\2\2\u009e"+
		"\13\3\2\2\2\u009f\u00a1\7\6\2\2\u00a0\u00a2\t\2\2\2\u00a1\u00a0\3\2\2"+
		"\2\u00a1\u00a2\3\2\2\2\u00a2\u00a4\3\2\2\2\u00a3\u00a5\7D\2\2\u00a4\u00a3"+
		"\3\2\2\2\u00a5\u00a6\3\2\2\2\u00a6\u00a4\3\2\2\2\u00a6\u00a7\3\2\2\2\u00a7"+
		"\r\3\2\2\2\u00a8\u00ab\5\b\5\2\u00a9\u00ab\5\n\6\2\u00aa\u00a8\3\2\2\2"+
		"\u00aa\u00a9\3\2\2\2\u00ab\17\3\2\2\2\u00ac\u00ad\5\6\4\2\u00ad\u00ae"+
		"\7\t\2\2\u00ae\u00af\5\22\n\2\u00af\21\3\2\2\2\u00b0\u00b1\5,\27\2\u00b1"+
		"\23\3\2\2\2\u00b2\u00b3\5\6\4\2\u00b3\u00b4\7\t\2\2\u00b4\u00b5\5\26\f"+
		"\2\u00b5\25\3\2\2\2\u00b6\u00bc\5\4\3\2\u00b7\u00bc\5\30\r\2\u00b8\u00bc"+
		"\5\34\17\2\u00b9\u00bc\5&\24\2\u00ba\u00bc\5(\25\2\u00bb\u00b6\3\2\2\2"+
		"\u00bb\u00b7\3\2\2\2\u00bb\u00b8\3\2\2\2\u00bb\u00b9\3\2\2\2\u00bb\u00ba"+
		"\3\2\2\2\u00bc\27\3\2\2\2\u00bd\u00be\7\37\2\2\u00be\u00c3\5\32\16\2\u00bf"+
		"\u00c0\7\n\2\2\u00c0\u00c2\5\32\16\2\u00c1\u00bf\3\2\2\2\u00c2\u00c5\3"+
		"\2\2\2\u00c3\u00c1\3\2\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c6\3\2\2\2\u00c5"+
		"\u00c3\3\2\2\2\u00c6\u00c7\7 \2\2\u00c7\u00c8\5\26\f\2\u00c8\31\3\2\2"+
		"\2\u00c9\u00ca\5\22\n\2\u00ca\33\3\2\2\2\u00cb\u00d0\7$\2\2\u00cc\u00cd"+
		"\7\13\2\2\u00cd\u00ce\5\36\20\2\u00ce\u00cf\7\f\2\2\u00cf\u00d1\3\2\2"+
		"\2\u00d0\u00cc\3\2\2\2\u00d0\u00d1\3\2\2\2\u00d1\u00d3\3\2\2\2\u00d2\u00d4"+
		"\5 \21\2\u00d3\u00d2\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4\u00d5\3\2\2\2\u00d5"+
		"\u00d6\7!\2\2\u00d6\35\3\2\2\2\u00d7\u00d8\5\4\3\2\u00d8\37\3\2\2\2\u00d9"+
		"\u00de\5\"\22\2\u00da\u00db\7\r\2\2\u00db\u00dd\5\"\22\2\u00dc\u00da\3"+
		"\2\2\2\u00dd\u00e0\3\2\2\2\u00de\u00dc\3\2\2\2\u00de\u00df\3\2\2\2\u00df"+
		"!\3\2\2\2\u00e0\u00de\3\2\2\2\u00e1\u00e2\5$\23\2\u00e2\u00e3\7\16\2\2"+
		"\u00e3\u00e4\5\26\f\2\u00e4#\3\2\2\2\u00e5\u00ea\5\6\4\2\u00e6\u00e7\7"+
		"\n\2\2\u00e7\u00e9\5\6\4\2\u00e8\u00e6\3\2\2\2\u00e9\u00ec\3\2\2\2\u00ea"+
		"\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb%\3\2\2\2\u00ec\u00ea\3\2\2\2"+
		"\u00ed\u00ee\7\"\2\2\u00ee\u00ef\7#\2\2\u00ef\u00f0\5\26\f\2\u00f0\'\3"+
		"\2\2\2\u00f1\u00f3\7%\2\2\u00f2\u00f4\5h\65\2\u00f3\u00f2\3\2\2\2\u00f3"+
		"\u00f4\3\2\2\2\u00f4)\3\2\2\2\u00f5\u00f6\5$\23\2\u00f6\u00f7\7\16\2\2"+
		"\u00f7\u00f8\5\26\f\2\u00f8+\3\2\2\2\u00f9\u00fd\5\60\31\2\u00fa\u00fb"+
		"\5.\30\2\u00fb\u00fc\5\60\31\2\u00fc\u00fe\3\2\2\2\u00fd\u00fa\3\2\2\2"+
		"\u00fd\u00fe\3\2\2\2\u00fe-\3\2\2\2\u00ff\u0100\t\3\2\2\u0100/\3\2\2\2"+
		"\u0101\u0103\t\2\2\2\u0102\u0101\3\2\2\2\u0102\u0103\3\2\2\2\u0103\u0104"+
		"\3\2\2\2\u0104\u010a\5\64\33\2\u0105\u0106\5\62\32\2\u0106\u0107\5\64"+
		"\33\2\u0107\u0109\3\2\2\2\u0108\u0105\3\2\2\2\u0109\u010c\3\2\2\2\u010a"+
		"\u0108\3\2\2\2\u010a\u010b\3\2\2\2\u010b\61\3\2\2\2\u010c\u010a\3\2\2"+
		"\2\u010d\u010e\t\4\2\2\u010e\63\3\2\2\2\u010f\u0115\58\35\2\u0110\u0111"+
		"\5\66\34\2\u0111\u0112\58\35\2\u0112\u0114\3\2\2\2\u0113\u0110\3\2\2\2"+
		"\u0114\u0117\3\2\2\2\u0115\u0113\3\2\2\2\u0115\u0116\3\2\2\2\u0116\65"+
		"\3\2\2\2\u0117\u0115\3\2\2\2\u0118\u0119\t\5\2\2\u0119\67\3\2\2\2\u011a"+
		"\u012b\5\16\b\2\u011b\u012b\7@\2\2\u011c\u012b\7+\2\2\u011d\u012b\7,\2"+
		"\2\u011e\u012b\7-\2\2\u011f\u012b\5> \2\u0120\u0122\5:\36\2\u0121\u0123"+
		"\5D#\2\u0122\u0121\3\2\2\2\u0122\u0123\3\2\2\2\u0123\u012b\3\2\2\2\u0124"+
		"\u0125\7\13\2\2\u0125\u0126\5,\27\2\u0126\u0127\7\f\2\2\u0127\u012b\3"+
		"\2\2\2\u0128\u0129\7\26\2\2\u0129\u012b\58\35\2\u012a\u011a\3\2\2\2\u012a"+
		"\u011b\3\2\2\2\u012a\u011c\3\2\2\2\u012a\u011d\3\2\2\2\u012a\u011e\3\2"+
		"\2\2\u012a\u011f\3\2\2\2\u012a\u0120\3\2\2\2\u012a\u0124\3\2\2\2\u012a"+
		"\u0128\3\2\2\2\u012b9\3\2\2\2\u012c\u0130\5\4\3\2\u012d\u012f\5<\37\2"+
		"\u012e\u012d\3\2\2\2\u012f\u0132\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u0131"+
		"\3\2\2\2\u0131;\3\2\2\2\u0132\u0130\3\2\2\2\u0133\u0134\7\3\2\2\u0134"+
		"\u013f\5\2\2\2\u0135\u0136\7\27\2\2\u0136\u0137\5B\"\2\u0137\u0138\7\30"+
		"\2\2\u0138\u013f\3\2\2\2\u0139\u013f\7\31\2\2\u013a\u013b\7\13\2\2\u013b"+
		"\u013c\5\4\3\2\u013c\u013d\7\f\2\2\u013d\u013f\3\2\2\2\u013e\u0133\3\2"+
		"\2\2\u013e\u0135\3\2\2\2\u013e\u0139\3\2\2\2\u013e\u013a\3\2\2\2\u013f"+
		"=\3\2\2\2\u0140\u0149\7\32\2\2\u0141\u0146\5@!\2\u0142\u0143\7\n\2\2\u0143"+
		"\u0145\5@!\2\u0144\u0142\3\2\2\2\u0145\u0148\3\2\2\2\u0146\u0144\3\2\2"+
		"\2\u0146\u0147\3\2\2\2\u0147\u014a\3\2\2\2\u0148\u0146\3\2\2\2\u0149\u0141"+
		"\3\2\2\2\u0149\u014a\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\7\33\2\2"+
		"\u014c?\3\2\2\2\u014d\u0150\5,\27\2\u014e\u014f\7\34\2\2\u014f\u0151\5"+
		",\27\2\u0150\u014e\3\2\2\2\u0150\u0151\3\2\2\2\u0151A\3\2\2\2\u0152\u0157"+
		"\5,\27\2\u0153\u0154\7\n\2\2\u0154\u0156\5,\27\2\u0155\u0153\3\2\2\2\u0156"+
		"\u0159\3\2\2\2\u0157\u0155\3\2\2\2\u0157\u0158\3\2\2\2\u0158C\3\2\2\2"+
		"\u0159\u0157\3\2\2\2\u015a\u015c\7\13\2\2\u015b\u015d\5B\"\2\u015c\u015b"+
		"\3\2\2\2\u015c\u015d\3\2\2\2\u015d\u015e\3\2\2\2\u015e\u015f\7\f\2\2\u015f"+
		"E\3\2\2\2\u0160\u0168\5H%\2\u0161\u0168\5J&\2\u0162\u0168\5N(\2\u0163"+
		"\u0168\5P)\2\u0164\u0168\5Z.\2\u0165\u0168\5\\/\2\u0166\u0168\5^\60\2"+
		"\u0167\u0160\3\2\2\2\u0167\u0161\3\2\2\2\u0167\u0162\3\2\2\2\u0167\u0163"+
		"\3\2\2\2\u0167\u0164\3\2\2\2\u0167\u0165\3\2\2\2\u0167\u0166\3\2\2\2\u0167"+
		"\u0168\3\2\2\2\u0168G\3\2\2\2\u0169\u016a\5:\36\2\u016a\u016b\7\35\2\2"+
		"\u016b\u016c\5,\27\2\u016cI\3\2\2\2\u016d\u016f\5:\36\2\u016e\u0170\5"+
		"D#\2\u016f\u016e\3\2\2\2\u016f\u0170\3\2\2\2\u0170K\3\2\2\2\u0171\u0176"+
		"\5F$\2\u0172\u0173\7\r\2\2\u0173\u0175\5F$\2\u0174\u0172\3\2\2\2\u0175"+
		"\u0178\3\2\2\2\u0176\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177M\3\2\2\2"+
		"\u0178\u0176\3\2\2\2\u0179\u017a\7.\2\2\u017a\u017b\5,\27\2\u017b\u017c"+
		"\7/\2\2\u017c\u0184\5L\'\2\u017d\u017e\7\60\2\2\u017e\u017f\5,\27\2\u017f"+
		"\u0180\7/\2\2\u0180\u0181\5L\'\2\u0181\u0183\3\2\2\2\u0182\u017d\3\2\2"+
		"\2\u0183\u0186\3\2\2\2\u0184\u0182\3\2\2\2\u0184\u0185\3\2\2\2\u0185\u0189"+
		"\3\2\2\2\u0186\u0184\3\2\2\2\u0187\u0188\7\61\2\2\u0188\u018a\5L\'\2\u0189"+
		"\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018b\3\2\2\2\u018b\u018c\7!"+
		"\2\2\u018cO\3\2\2\2\u018d\u018e\7\62\2\2\u018e\u018f\5,\27\2\u018f\u0190"+
		"\7 \2\2\u0190\u0195\5R*\2\u0191\u0192\7\36\2\2\u0192\u0194\5R*\2\u0193"+
		"\u0191\3\2\2\2\u0194\u0197\3\2\2\2\u0195\u0193\3\2\2\2\u0195\u0196\3\2"+
		"\2\2\u0196\u0198\3\2\2\2\u0197\u0195\3\2\2\2\u0198\u0199\7!\2\2\u0199"+
		"Q\3\2\2\2\u019a\u019b\5T+\2\u019b\u019c\7\16\2\2\u019c\u019d\5L\'\2\u019d"+
		"\u019f\3\2\2\2\u019e\u019a\3\2\2\2\u019e\u019f\3\2\2\2\u019fS\3\2\2\2"+
		"\u01a0\u01a5\5V,\2\u01a1\u01a2\7\n\2\2\u01a2\u01a4\5V,\2\u01a3\u01a1\3"+
		"\2\2\2\u01a4\u01a7\3\2\2\2\u01a5\u01a3\3\2\2\2\u01a5\u01a6\3\2\2\2\u01a6"+
		"U\3\2\2\2\u01a7\u01a5\3\2\2\2\u01a8\u01ab\5X-\2\u01a9\u01aa\7\34\2\2\u01aa"+
		"\u01ac\5X-\2\u01ab\u01a9\3\2\2\2\u01ab\u01ac\3\2\2\2\u01acW\3\2\2\2\u01ad"+
		"\u01b1\5\b\5\2\u01ae\u01b1\7@\2\2\u01af\u01b1\5\4\3\2\u01b0\u01ad\3\2"+
		"\2\2\u01b0\u01ae\3\2\2\2\u01b0\u01af\3\2\2\2\u01b1Y\3\2\2\2\u01b2\u01b3"+
		"\7\63\2\2\u01b3\u01b4\5,\27\2\u01b4\u01b5\7\64\2\2\u01b5\u01bd\5L\'\2"+
		"\u01b6\u01b7\7\60\2\2\u01b7\u01b8\5,\27\2\u01b8\u01b9\7\64\2\2\u01b9\u01ba"+
		"\5L\'\2\u01ba\u01bc\3\2\2\2\u01bb\u01b6\3\2\2\2\u01bc\u01bf\3\2\2\2\u01bd"+
		"\u01bb\3\2\2\2\u01bd\u01be\3\2\2\2\u01be\u01c0\3\2\2\2\u01bf\u01bd\3\2"+
		"\2\2\u01c0\u01c1\7!\2\2\u01c1[\3\2\2\2\u01c2\u01c3\7\65\2\2\u01c3\u01c4"+
		"\5L\'\2\u01c4\u01c5\7\66\2\2\u01c5\u01c6\5,\27\2\u01c6]\3\2\2\2\u01c7"+
		"\u01c8\7\67\2\2\u01c8\u01c9\5\2\2\2\u01c9\u01ca\7\35\2\2\u01ca\u01cb\5"+
		",\27\2\u01cb\u01cc\7#\2\2\u01cc\u01cf\5,\27\2\u01cd\u01ce\78\2\2\u01ce"+
		"\u01d0\5\22\n\2\u01cf\u01cd\3\2\2\2\u01cf\u01d0\3\2\2\2\u01d0\u01d1\3"+
		"\2\2\2\u01d1\u01d2\7\64\2\2\u01d2\u01d3\5L\'\2\u01d3\u01d4\7!\2\2\u01d4"+
		"_\3\2\2\2\u01d5\u01d6\5b\62\2\u01d6\u01d7\7\r\2\2\u01d7\u01d8\5d\63\2"+
		"\u01d8\u01d9\5\2\2\2\u01d9a\3\2\2\2\u01da\u01db\7%\2\2\u01db\u01dd\5\6"+
		"\4\2\u01dc\u01de\5h\65\2\u01dd\u01dc\3\2\2\2\u01dd\u01de\3\2\2\2\u01de"+
		"c\3\2\2\2\u01df\u01e2\5f\64\2\u01e0\u01e1\79\2\2\u01e1\u01e3\5L\'\2\u01e2"+
		"\u01e0\3\2\2\2\u01e2\u01e3\3\2\2\2\u01e3\u01e6\3\2\2\2\u01e4\u01e5\7:"+
		"\2\2\u01e5\u01e7\5,\27\2\u01e6\u01e4\3\2\2\2\u01e6\u01e7\3\2\2\2\u01e7"+
		"\u01e8\3\2\2\2\u01e8\u01e9\7!\2\2\u01e9e\3\2\2\2\u01ea\u01f0\7;\2\2\u01eb"+
		"\u01ec\5\20\t\2\u01ec\u01ed\7\r\2\2\u01ed\u01ef\3\2\2\2\u01ee\u01eb\3"+
		"\2\2\2\u01ef\u01f2\3\2\2\2\u01f0\u01ee\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1"+
		"\u01f4\3\2\2\2\u01f2\u01f0\3\2\2\2\u01f3\u01ea\3\2\2\2\u01f3\u01f4\3\2"+
		"\2\2\u01f4\u01fe\3\2\2\2\u01f5\u01fb\7<\2\2\u01f6\u01f7\5\24\13\2\u01f7"+
		"\u01f8\7\r\2\2\u01f8\u01fa\3\2\2\2\u01f9\u01f6\3\2\2\2\u01fa\u01fd\3\2"+
		"\2\2\u01fb\u01f9\3\2\2\2\u01fb\u01fc\3\2\2\2\u01fc\u01ff\3\2\2\2\u01fd"+
		"\u01fb\3\2\2\2\u01fe\u01f5\3\2\2\2\u01fe\u01ff\3\2\2\2\u01ff\u0209\3\2"+
		"\2\2\u0200\u0206\7=\2\2\u0201\u0202\5*\26\2\u0202\u0203\7\r\2\2\u0203"+
		"\u0205\3\2\2\2\u0204\u0201\3\2\2\2\u0205\u0208\3\2\2\2\u0206\u0204\3\2"+
		"\2\2\u0206\u0207\3\2\2\2\u0207\u020a\3\2\2\2\u0208\u0206\3\2\2\2\u0209"+
		"\u0200\3\2\2\2\u0209\u020a\3\2\2\2\u020a\u0210\3\2\2\2\u020b\u020c\5`"+
		"\61\2\u020c\u020d\7\r\2\2\u020d\u020f\3\2\2\2\u020e\u020b\3\2\2\2\u020f"+
		"\u0212\3\2\2\2\u0210\u020e\3\2\2\2\u0210\u0211\3\2\2\2\u0211g\3\2\2\2"+
		"\u0212\u0210\3\2\2\2\u0213\u021c\7\13\2\2\u0214\u0219\5j\66\2\u0215\u0216"+
		"\7\r\2\2\u0216\u0218\5j\66\2\u0217\u0215\3\2\2\2\u0218\u021b\3\2\2\2\u0219"+
		"\u0217\3\2\2\2\u0219\u021a\3\2\2\2\u021a\u021d\3\2\2\2\u021b\u0219\3\2"+
		"\2\2\u021c\u0214\3\2\2\2\u021c\u021d\3\2\2\2\u021d\u021e\3\2\2\2\u021e"+
		"\u0221\7\f\2\2\u021f\u0220\7\16\2\2\u0220\u0222\5\4\3\2\u0221\u021f\3"+
		"\2\2\2\u0221\u0222\3\2\2\2\u0222i\3\2\2\2\u0223\u0225\7=\2\2\u0224\u0223"+
		"\3\2\2\2\u0224\u0225\3\2\2\2\u0225\u0226\3\2\2\2\u0226\u022b\5\2\2\2\u0227"+
		"\u0228\7\n\2\2\u0228\u022a\5\2\2\2\u0229\u0227\3\2\2\2\u022a\u022d\3\2"+
		"\2\2\u022b\u0229\3\2\2\2\u022b\u022c\3\2\2\2\u022c\u022e\3\2\2\2\u022d"+
		"\u022b\3\2\2\2\u022e\u022f\7\16\2\2\u022f\u0230\5l\67\2\u0230k\3\2\2\2"+
		"\u0231\u0232\7\37\2\2\u0232\u0234\7 \2\2\u0233\u0231\3\2\2\2\u0234\u0237"+
		"\3\2\2\2\u0235\u0233\3\2\2\2\u0235\u0236\3\2\2\2\u0236\u0238\3\2\2\2\u0237"+
		"\u0235\3\2\2\2\u0238\u0239\5\4\3\2\u0239m\3\2\2\2\u023a\u023b\7>\2\2\u023b"+
		"\u023c\5\2\2\2\u023c\u023e\7\r\2\2\u023d\u023f\5p9\2\u023e\u023d\3\2\2"+
		"\2\u023e\u023f\3\2\2\2\u023f\u0240\3\2\2\2\u0240\u0243\5f\64\2\u0241\u0242"+
		"\79\2\2\u0242\u0244\5L\'\2\u0243\u0241\3\2\2\2\u0243\u0244\3\2\2\2\u0244"+
		"\u0245\3\2\2\2\u0245\u0246\7!\2\2\u0246\u0247\5\2\2\2\u0247\u0248\7\3"+
		"\2\2\u0248o\3\2\2\2\u0249\u024a\7?\2\2\u024a\u024f\5r:\2\u024b\u024c\7"+
		"\n\2\2\u024c\u024e\5r:\2\u024d\u024b\3\2\2\2\u024e\u0251\3\2\2\2\u024f"+
		"\u024d\3\2\2\2\u024f\u0250\3\2\2\2\u0250\u0252\3\2\2\2\u0251\u024f\3\2"+
		"\2\2\u0252\u0253\7\r\2\2\u0253q\3\2\2\2\u0254\u0257\5\2\2\2\u0255\u0256"+
		"\7\35\2\2\u0256\u0258\5\2\2\2\u0257\u0255\3\2\2\2\u0257\u0258\3\2\2\2"+
		"\u0258s\3\2\2\2Ay\177\u0084\u008a\u008e\u0093\u0099\u009d\u00a1\u00a6"+
		"\u00aa\u00bb\u00c3\u00d0\u00d3\u00de\u00ea\u00f3\u00fd\u0102\u010a\u0115"+
		"\u0122\u012a\u0130\u013e\u0146\u0149\u0150\u0157\u015c\u0167\u016f\u0176"+
		"\u0184\u0189\u0195\u019e\u01a5\u01ab\u01b0\u01bd\u01cf\u01dd\u01e2\u01e6"+
		"\u01f0\u01f3\u01fb\u01fe\u0206\u0209\u0210\u0219\u021c\u0221\u0224\u022b"+
		"\u0235\u023e\u0243\u024f\u0257";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}