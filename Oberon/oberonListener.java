// Generated from oberon.g4 by ANTLR 4.5.3
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link oberonParser}.
 */
public interface oberonListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link oberonParser#ident}.
	 * @param ctx the parse tree
	 */
	void enterIdent(oberonParser.IdentContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#ident}.
	 * @param ctx the parse tree
	 */
	void exitIdent(oberonParser.IdentContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#qualident}.
	 * @param ctx the parse tree
	 */
	void enterQualident(oberonParser.QualidentContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#qualident}.
	 * @param ctx the parse tree
	 */
	void exitQualident(oberonParser.QualidentContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#identdef}.
	 * @param ctx the parse tree
	 */
	void enterIdentdef(oberonParser.IdentdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#identdef}.
	 * @param ctx the parse tree
	 */
	void exitIdentdef(oberonParser.IdentdefContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#integer}.
	 * @param ctx the parse tree
	 */
	void enterInteger(oberonParser.IntegerContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#integer}.
	 * @param ctx the parse tree
	 */
	void exitInteger(oberonParser.IntegerContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#real}.
	 * @param ctx the parse tree
	 */
	void enterReal(oberonParser.RealContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#real}.
	 * @param ctx the parse tree
	 */
	void exitReal(oberonParser.RealContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#scaleFactor}.
	 * @param ctx the parse tree
	 */
	void enterScaleFactor(oberonParser.ScaleFactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#scaleFactor}.
	 * @param ctx the parse tree
	 */
	void exitScaleFactor(oberonParser.ScaleFactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#number}.
	 * @param ctx the parse tree
	 */
	void enterNumber(oberonParser.NumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#number}.
	 * @param ctx the parse tree
	 */
	void exitNumber(oberonParser.NumberContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#constDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterConstDeclaration(oberonParser.ConstDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#constDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitConstDeclaration(oberonParser.ConstDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#constExpression}.
	 * @param ctx the parse tree
	 */
	void enterConstExpression(oberonParser.ConstExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#constExpression}.
	 * @param ctx the parse tree
	 */
	void exitConstExpression(oberonParser.ConstExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterTypeDeclaration(oberonParser.TypeDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#typeDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitTypeDeclaration(oberonParser.TypeDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(oberonParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(oberonParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#arrayType}.
	 * @param ctx the parse tree
	 */
	void enterArrayType(oberonParser.ArrayTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#arrayType}.
	 * @param ctx the parse tree
	 */
	void exitArrayType(oberonParser.ArrayTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#length}.
	 * @param ctx the parse tree
	 */
	void enterLength(oberonParser.LengthContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#length}.
	 * @param ctx the parse tree
	 */
	void exitLength(oberonParser.LengthContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#recordType}.
	 * @param ctx the parse tree
	 */
	void enterRecordType(oberonParser.RecordTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#recordType}.
	 * @param ctx the parse tree
	 */
	void exitRecordType(oberonParser.RecordTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#baseType}.
	 * @param ctx the parse tree
	 */
	void enterBaseType(oberonParser.BaseTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#baseType}.
	 * @param ctx the parse tree
	 */
	void exitBaseType(oberonParser.BaseTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#fieldListSequence}.
	 * @param ctx the parse tree
	 */
	void enterFieldListSequence(oberonParser.FieldListSequenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#fieldListSequence}.
	 * @param ctx the parse tree
	 */
	void exitFieldListSequence(oberonParser.FieldListSequenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#fieldList}.
	 * @param ctx the parse tree
	 */
	void enterFieldList(oberonParser.FieldListContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#fieldList}.
	 * @param ctx the parse tree
	 */
	void exitFieldList(oberonParser.FieldListContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#identList}.
	 * @param ctx the parse tree
	 */
	void enterIdentList(oberonParser.IdentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#identList}.
	 * @param ctx the parse tree
	 */
	void exitIdentList(oberonParser.IdentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#pointerType}.
	 * @param ctx the parse tree
	 */
	void enterPointerType(oberonParser.PointerTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#pointerType}.
	 * @param ctx the parse tree
	 */
	void exitPointerType(oberonParser.PointerTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#procedureType}.
	 * @param ctx the parse tree
	 */
	void enterProcedureType(oberonParser.ProcedureTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#procedureType}.
	 * @param ctx the parse tree
	 */
	void exitProcedureType(oberonParser.ProcedureTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVariableDeclaration(oberonParser.VariableDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#variableDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVariableDeclaration(oberonParser.VariableDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(oberonParser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(oberonParser.ExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#relation}.
	 * @param ctx the parse tree
	 */
	void enterRelation(oberonParser.RelationContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#relation}.
	 * @param ctx the parse tree
	 */
	void exitRelation(oberonParser.RelationContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void enterSimpleExpression(oberonParser.SimpleExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#simpleExpression}.
	 * @param ctx the parse tree
	 */
	void exitSimpleExpression(oberonParser.SimpleExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#addOperator}.
	 * @param ctx the parse tree
	 */
	void enterAddOperator(oberonParser.AddOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#addOperator}.
	 * @param ctx the parse tree
	 */
	void exitAddOperator(oberonParser.AddOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#term}.
	 * @param ctx the parse tree
	 */
	void enterTerm(oberonParser.TermContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#term}.
	 * @param ctx the parse tree
	 */
	void exitTerm(oberonParser.TermContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#mulOperator}.
	 * @param ctx the parse tree
	 */
	void enterMulOperator(oberonParser.MulOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#mulOperator}.
	 * @param ctx the parse tree
	 */
	void exitMulOperator(oberonParser.MulOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#factor}.
	 * @param ctx the parse tree
	 */
	void enterFactor(oberonParser.FactorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#factor}.
	 * @param ctx the parse tree
	 */
	void exitFactor(oberonParser.FactorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#designator}.
	 * @param ctx the parse tree
	 */
	void enterDesignator(oberonParser.DesignatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#designator}.
	 * @param ctx the parse tree
	 */
	void exitDesignator(oberonParser.DesignatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#selector}.
	 * @param ctx the parse tree
	 */
	void enterSelector(oberonParser.SelectorContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#selector}.
	 * @param ctx the parse tree
	 */
	void exitSelector(oberonParser.SelectorContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#set}.
	 * @param ctx the parse tree
	 */
	void enterSet(oberonParser.SetContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#set}.
	 * @param ctx the parse tree
	 */
	void exitSet(oberonParser.SetContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#element}.
	 * @param ctx the parse tree
	 */
	void enterElement(oberonParser.ElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#element}.
	 * @param ctx the parse tree
	 */
	void exitElement(oberonParser.ElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#expList}.
	 * @param ctx the parse tree
	 */
	void enterExpList(oberonParser.ExpListContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#expList}.
	 * @param ctx the parse tree
	 */
	void exitExpList(oberonParser.ExpListContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#actualParameters}.
	 * @param ctx the parse tree
	 */
	void enterActualParameters(oberonParser.ActualParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#actualParameters}.
	 * @param ctx the parse tree
	 */
	void exitActualParameters(oberonParser.ActualParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(oberonParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(oberonParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void enterAssignment(oberonParser.AssignmentContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#assignment}.
	 * @param ctx the parse tree
	 */
	void exitAssignment(oberonParser.AssignmentContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#procedureCall}.
	 * @param ctx the parse tree
	 */
	void enterProcedureCall(oberonParser.ProcedureCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#procedureCall}.
	 * @param ctx the parse tree
	 */
	void exitProcedureCall(oberonParser.ProcedureCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#statementSequence}.
	 * @param ctx the parse tree
	 */
	void enterStatementSequence(oberonParser.StatementSequenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#statementSequence}.
	 * @param ctx the parse tree
	 */
	void exitStatementSequence(oberonParser.StatementSequenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(oberonParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(oberonParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void enterCaseStatement(oberonParser.CaseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#caseStatement}.
	 * @param ctx the parse tree
	 */
	void exitCaseStatement(oberonParser.CaseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#case_}.
	 * @param ctx the parse tree
	 */
	void enterCase_(oberonParser.Case_Context ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#case_}.
	 * @param ctx the parse tree
	 */
	void exitCase_(oberonParser.Case_Context ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#caseLabelList}.
	 * @param ctx the parse tree
	 */
	void enterCaseLabelList(oberonParser.CaseLabelListContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#caseLabelList}.
	 * @param ctx the parse tree
	 */
	void exitCaseLabelList(oberonParser.CaseLabelListContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#labelRange}.
	 * @param ctx the parse tree
	 */
	void enterLabelRange(oberonParser.LabelRangeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#labelRange}.
	 * @param ctx the parse tree
	 */
	void exitLabelRange(oberonParser.LabelRangeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#label}.
	 * @param ctx the parse tree
	 */
	void enterLabel(oberonParser.LabelContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#label}.
	 * @param ctx the parse tree
	 */
	void exitLabel(oberonParser.LabelContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(oberonParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(oberonParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#repeatStatement}.
	 * @param ctx the parse tree
	 */
	void enterRepeatStatement(oberonParser.RepeatStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#repeatStatement}.
	 * @param ctx the parse tree
	 */
	void exitRepeatStatement(oberonParser.RepeatStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(oberonParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(oberonParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#procedureDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterProcedureDeclaration(oberonParser.ProcedureDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#procedureDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitProcedureDeclaration(oberonParser.ProcedureDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#procedureHeading}.
	 * @param ctx the parse tree
	 */
	void enterProcedureHeading(oberonParser.ProcedureHeadingContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#procedureHeading}.
	 * @param ctx the parse tree
	 */
	void exitProcedureHeading(oberonParser.ProcedureHeadingContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#procedureBody}.
	 * @param ctx the parse tree
	 */
	void enterProcedureBody(oberonParser.ProcedureBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#procedureBody}.
	 * @param ctx the parse tree
	 */
	void exitProcedureBody(oberonParser.ProcedureBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#declarationSequence}.
	 * @param ctx the parse tree
	 */
	void enterDeclarationSequence(oberonParser.DeclarationSequenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#declarationSequence}.
	 * @param ctx the parse tree
	 */
	void exitDeclarationSequence(oberonParser.DeclarationSequenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameters(oberonParser.FormalParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#formalParameters}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameters(oberonParser.FormalParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#fPSection}.
	 * @param ctx the parse tree
	 */
	void enterFPSection(oberonParser.FPSectionContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#fPSection}.
	 * @param ctx the parse tree
	 */
	void exitFPSection(oberonParser.FPSectionContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#formalType}.
	 * @param ctx the parse tree
	 */
	void enterFormalType(oberonParser.FormalTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#formalType}.
	 * @param ctx the parse tree
	 */
	void exitFormalType(oberonParser.FormalTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#module}.
	 * @param ctx the parse tree
	 */
	void enterModule(oberonParser.ModuleContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#module}.
	 * @param ctx the parse tree
	 */
	void exitModule(oberonParser.ModuleContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#importList}.
	 * @param ctx the parse tree
	 */
	void enterImportList(oberonParser.ImportListContext ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#importList}.
	 * @param ctx the parse tree
	 */
	void exitImportList(oberonParser.ImportListContext ctx);
	/**
	 * Enter a parse tree produced by {@link oberonParser#import_}.
	 * @param ctx the parse tree
	 */
	void enterImport_(oberonParser.Import_Context ctx);
	/**
	 * Exit a parse tree produced by {@link oberonParser#import_}.
	 * @param ctx the parse tree
	 */
	void exitImport_(oberonParser.Import_Context ctx);
}